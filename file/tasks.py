from celery import shared_task

from file.models import File


@shared_task
def process_file(instance_id):
    try:
        file = File.objects.get(id=instance_id)
        file.processed = True
        file.save()
    except File.DoesNotExist:
        pass

