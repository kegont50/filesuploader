from rest_framework import mixins, generics
from .models import File
from .serializers import FileSerializer
from .tasks import process_file


class FileUploadView(generics.CreateAPIView):
    serializer_class = FileSerializer

    def perform_create(self, serializer):
        instance = serializer.save()
        process_file.apply_async(args=[instance.id])


class FileListView(generics.ListAPIView):
    queryset = File.objects.all()
    serializer_class = FileSerializer
